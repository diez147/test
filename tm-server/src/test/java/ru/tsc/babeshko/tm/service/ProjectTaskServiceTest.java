package ru.tsc.babeshko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.babeshko.tm.api.service.IConnectionService;
import ru.tsc.babeshko.tm.api.service.IPropertyService;
import ru.tsc.babeshko.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.babeshko.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.tsc.babeshko.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.babeshko.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.babeshko.tm.dto.model.ProjectDTO;
import ru.tsc.babeshko.tm.dto.model.TaskDTO;
import ru.tsc.babeshko.tm.dto.model.UserDTO;
import ru.tsc.babeshko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.babeshko.tm.exception.field.EmptyIdException;
import ru.tsc.babeshko.tm.exception.field.EmptyUserIdException;
import ru.tsc.babeshko.tm.service.dto.ProjectServiceDTO;
import ru.tsc.babeshko.tm.service.dto.ProjectTaskServiceDTO;
import ru.tsc.babeshko.tm.service.dto.TaskServiceDTO;
import ru.tsc.babeshko.tm.service.dto.UserServiceDTO;

public class ProjectTaskServiceTest {

    @NotNull
    private IProjectTaskServiceDTO projectTaskService;

    @NotNull
    private IUserServiceDTO userService;

    @NotNull
    private IProjectServiceDTO projectService;

    @NotNull
    private ITaskServiceDTO taskService;

    private String userId;

    private String projectId;

    private String taskId;

    @Before
    public void init() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        projectService = new ProjectServiceDTO(connectionService);
        taskService = new TaskServiceDTO(connectionService);
        projectTaskService = new ProjectTaskServiceDTO(connectionService, projectService, taskService);
        userService = new UserServiceDTO(connectionService, propertyService, taskService, projectService);
        @NotNull final UserDTO user = userService.create("user", "user");
        userId = user.getId();
        @NotNull final ProjectDTO project = projectService.create(userId, "project");
        projectId = project.getId();
        @NotNull final TaskDTO task = taskService.create(userId, "task");
        taskId = task.getId();
    }

    @After
    public void end() {
        taskService.clear(userId);
        projectService.clear(userId);
        userService.removeByLogin("user");
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectTaskService.bindTaskToProject("", projectId, taskId));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.bindTaskToProject(userId, "", taskId));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.bindTaskToProject(userId, projectId, ""));
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(userId, projectId, "not_task_id"));

        projectTaskService.bindTaskToProject(userId, projectId, taskId);
        @NotNull final TaskDTO task = taskService.findOneById(taskId);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(projectId, task.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectTaskService.unbindTaskFromProject("", projectId, taskId));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.unbindTaskFromProject(userId, "", taskId));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.unbindTaskFromProject(userId, projectId, ""));
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.unbindTaskFromProject(userId, projectId, "not_task_id"));
        projectTaskService.unbindTaskFromProject(userId, projectId, taskId);
        @NotNull final TaskDTO task = taskService.findOneById(taskId);
        Assert.assertNull(task.getProjectId());
    }

}