package ru.tsc.babeshko.tm.api.repository.dto;

import ru.tsc.babeshko.tm.dto.model.ProjectDTO;

public interface IProjectRepositoryDTO extends IUserOwnedRepositoryDTO<ProjectDTO> {

    void removeAllByUserId(String userId);

}
